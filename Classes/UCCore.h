//
//  UCMain.h
//  UrTime
//
//  Created by Andrew on 1/10/17.
//  Copyright © 2017 nlt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UUNotificationManager.h"

#import "UrMUser.h"
#import "UrMEvent.h"
#import "URMTag.h"

#import "UVCParent.h"
#import "UVCIdentification.h"
#import "UVCPictureGallery.h"
#import "UVCLogin.h"
#import "UVCNewUser.h"
#import "UVCHomeFeed.h"
#import "UVCEvent.h"

#import "UVCEventDetails.h"
#import "UVCContacts.h"
#import "UVCMusic.h"
#import "UVCRestorePassword.h"
#import "USettingsViewController.h"

@interface UCCore : NSObject
+ (UCCore *) sharedController;


@property(nonatomic, strong)NSString *externalEventId;
@property(nonatomic, strong)UrMEvent *eventForDetails;

//@property (nonatomic, strong) SPTAudioStreamingController *player;
@property(weak)UIButton *playButton;
@property(strong)UIButton *notifyButton;

#pragma mark ViewControllers
@property(weak)UVCIdentification *vcIdentification;
@property(weak)UVCLogin *vcLogin;
@property(weak)UVCNewUser *vcNewUser;
@property(weak)UVCHomeFeed *vcHome;
@property(weak)UVCEvent *vcEvent;
@property(weak)UVCEventDetails *vcEventDetails;
@property(weak)UVCContacts *vcContacts;
@property(weak)UVCMusic *vcMusic;
@property(weak)UVCPictureGallery *vcPictureGallery;
@property(weak)UVCParent *currentViewConttoller;
@property(weak)UVCRestorePassword *vcRestorePassword;
@property(weak)USettingsViewController *vcSettings;

@property BOOL isPlayButtonNeedToShow;

@property(weak) UIViewController* vcPending;

#pragma mark Login methods
- (BOOL)isLogined;
- (BOOL)isOnline;


- (void)loginWithData:(NSDictionary*)userData;
- (void)loginExternalWithData:(NSDictionary*)userData;
- (void)didLogined:(UrMUser *)user;
//- (void)didLoginedWithName:(NSString *)name andPassword:(NSString *)password;
- (void)didLoginFailed:(NSString*)statusMessage;
- (void)didRegistrationLogined:(UrMUser *)user;
- (void)didRegistrationLoginFailed;
- (void)logout;
- (void)setLogined:(BOOL)status;
- (void)loadSavedUser;
- (void)testAppl;
- (void)sendRestorePasswordForEmail:(NSString*)email completionHandler:(void (^)(NSString *status, NSString *message))handler;
- (void)sendChangePasswordWithData:(NSDictionary*)userData completionHandler:(void (^)(NSString *status, NSString *message))handler;

#pragma mark - Contacts
-(void)searchContactsFor:(NSString*)name;
-(void)didReceivedContacts:(NSArray*)contacts;
-(void)contactsNotFound;
-(void)addContacts;

#pragma mark - Settings Title model methods

- (NSArray*)getSettingTitleDataForUser;
- (NSString*)getVCNameBySelectedChoice:(NSString*)choiseName;

#pragma mark - Music
-(void)searchTracksFor:(NSString*)name;
-(void)loadTracksForEventId:(NSString *)eventId andUserId:(NSString *)ownerId completionHandler:(void (^)(NSMutableArray *tracks))handler;
-(void)sendMusicItem:(NSMutableArray*)musicItems forEventId:(NSString*)evenrId completionHandler:(void (^)(NSString *status, NSString *message))handler;
-(void)addMusicItem:(UrMMusic*)musicItem forEventId:(NSString*)evenrId completionHandler:(void (^)(NSString *status, NSString *message))handler;
-(void)deleteMusicItem:(NSString*)musicItemId andUserId:(NSString*)userId completionHandler:(void (^)(NSString *status, NSString *message))handler;
-(void)approveMusicItem:(UrMMusic*)musicItem forEventId:(NSString*)evenrId completionHandler:(void (^)(NSString *status, NSString *message))handler;

#pragma mark - User Notifications
- (void)loadNotificationsForUser:(NSString *)userId completionHandler:(void (^)(NSMutableArray *notifications))handler;

#pragma mark - Events
-(void)registerEvent:(UrMEvent*)event;
-(void)didRegisterEvent:(NSString*)message;
-(void)didRegisterEventFailed:(NSString*)message;

-(void)loadEventsForUser;
-(void)didLoadEvents:(NSMutableArray*)eventsList;
-(void)didLoadEventsEmpty;

-(void)eventForLoadDetails:(UrMEvent *)event;
-(void)cleanEventForDetails;
-(void)loadEventDetails;
-(void)didLoadEventDetails:(UrMEvent *)event;

-(void)changeInviteStatus:(NSString*)status forUser:(NSString*)userId forEvent:(NSString*)eventId;
-(void)inviteStatusUpdated;
-(void)inviteStatusUpdatedFailed;

-(void)editEvent:(UrMEvent*)event;
-(void)didEditedEvent;
-(void)didEditedEventFailed:(NSString*)message;
-(void)deleteEventWithUserId:(NSString*)userId andEventId:(NSString*)eventId;
-(void)didEventDeleted;
-(void)didEventDeletedFiled:(NSString*)message;



#pragma mark - UploadImages
-(void) sendPictures:(NSMutableArray*)pictures forEventId:(NSString*)evenrId;

-(void)uploadPictureDone;
-(void)uploadPictureFiled:(NSString*)message;
-(void)sendPictureState:(UrMPicture*)pictureEntity;
-(void)pictureStateUpdated;
-(void)pictureStateUpdatedFiled:(NSString*)message;
-(void)sendPictureState:(UrMPicture*)pictureEntity completionHandler:(void (^)(NSString *status, NSString *message))handler;

-(void)deletePicture:(NSString*)imageId;
-(void)didPictureDeleted;
-(void)didPictureDeletedFiled:(NSString*)message;

#pragma mark - Profile
-(void)saveAvatarPicture:(UIImage*)avatarPicture;
-(UIImage*)loadAvatarPicture;
- (void)saveUserProfileWithData:(NSDictionary*)userData;

#pragma mark -
- (void)registerUserWithData:(NSDictionary*)userData;

- (NSDate *)dateAtEndingOfDayForDate:(NSDate *)inputDate;
- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate;
- (NSDate *)dateWithTime:(NSString*)timeString fromDate:(NSDate *)inputDate ;

- (NSDate *)dateWithEndHourTime:(NSString*)timeString fromDate:(NSDate *)inputDate ;



-(void)sendPushBindForUserId: (NSString*)userId
             andUserDeviceId: (NSString*)deviceId
               andDeviceType: (NSString*)deviceType;

#pragma mark - Notifications
-(void)checkNotification;

#pragma mark - Tags
-(void)getUserTags:(NSString*)userID completionHandler:(void (^)(NSMutableArray *tags))handler;
-(void)getEventTags:(NSString*)eventID completionHandler:(void (^)(NSMutableArray *tags))handler;
-(void)action:(int)actionCode forTags:(NSMutableArray*)tags completionHandler:(void (^)(NSString *status, NSString *message))handler;
-(void)action:(int)actionCode forTags:(NSMutableArray*)tags forEvent:(NSString*)eventID completionHandler:(void (^)(NSString *status, NSString *message))handler;

#pragma mark - Calendar
-(void)didloadCalendarEvents:(NSMutableArray*)events;
-(void)getUserCalendarTime:(NSDate*)date;

#pragma mark - Utils
-(UIImage*)loadImageFromURL:(NSString*)url;

-(void)loadButtonImage:(UIButton*)button forPicture:(UrMPicture*)picture;
-(void)loadViewImage:(UIImageView*)imageView forPicture:(UrMPicture*)picture;

-(void)loadImageByURL:(NSString*)url forButton:(UIButton*)button;
-(void)loadImageByURL:(NSString*)url forImageView:(UIImageView*)imageView;
-(void)load:(UIImageView *)imageView imageWithUrlString:(NSString *)urlString completionHandler:(void (^)(UIImage *image))handler;
-(NSInteger) correctPictureCellIndex:(NSInteger)rawCellIndex for:(NSMutableArray*)pictures;
-(NSMutableArray*) clearPicturesForInitialAndAvatar:(NSMutableArray*)pictures;

-(UIButton*)sharedPlayButton;

#pragma mark - Background Tasks
-(void)startBackgroundTasks;
-(void)didLoadNotificationsCount:(int)count;

@end













