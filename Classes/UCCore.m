//
//  UCMain.m
//  UrTime
//
//  Created by Andrew on 1/10/17.
//  Copyright © 2017 nlt. All rights reserved.
//

#import "UCCore.h"
#import "UCDataBase.h"
#import "CalendarManager.h"
#import "UCServerInteractor.h"
#import "UUReachabilityConnection.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <EventKit/EventKit.h>
#import <OneSignal/OneSignal.h>
#import "UrImageAsyncLoader.h"
#import "SettingsTitleModel.h"


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@interface UCCore ()
@property (nonatomic) UUReachabilityConnection *hostReachability;
@property (nonatomic) NSUserDefaults *preferences;
@property (nonatomic) NSMutableArray *eventsOnThisDay;
@property (nonatomic) UIActivityIndicatorView *spinner;
@property (nonatomic) NSInteger imagesToUpload;
@property (nonatomic) SettingsTitleModel* settingsTitleModel;
@end

@implementation UCCore{
    UCDataBase *database;
    BOOL bgTasksEnabled;
}

static UCCore *sharedController = nil;

+(UCCore *) sharedController{
    
    if (!sharedController) {
        sharedController = [[UCCore alloc] init];
    }
    return sharedController;
}

-(id)init{
    if (!sharedController) {
        sharedController = [super init];
        [sharedController checkInternetConnection];
        self.preferences = [NSUserDefaults standardUserDefaults];
        database = [UCDataBase sharedDataBase];
        self.isPlayButtonNeedToShow = NO;
        bgTasksEnabled = YES;
        [self startBackgroundTasks];
        _settingsTitleModel = [[SettingsTitleModel alloc] init];
 //       [self logout];
    }
    return sharedController;
}

-(UIButton*)sharedPlayButton{
//    if (!self.playButton) self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];    
    return self.playButton;
}

#pragma mark - Logining

-(void)testAppl{
    [[UCServerInteractor sharedServerInteractor] testApplicationWithCompletionHandler:^(NSString *response) {
        DLog(@"Response = %@",response);
    }];
}

- (void)loginWithData:(NSDictionary*)userData{
    [self showSpinner:self.vcLogin];
    [database loginWithData:userData];
}

- (void)loginExternalWithData:(NSDictionary*)userData{
    [self showSpinner:self.vcLogin];
    [database loginExternalWithData:userData];
}

- (void)didLogined:(UrMUser *)user{
    if (user) {
        NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
        
        [self.preferences setObject:encodedObject forKey:kUser];
        [self setLogined:YES];
        
        //for push notification
        [self bindUserToDevice:user];
        [self hideSpinner];
        if (!self.vcLogin) {
            UIStoryboard*  storyboard = [UIStoryboard storyboardWithName: @"Main" bundle: nil];
            self.vcLogin = (UVCLogin*)[storyboard instantiateViewControllerWithIdentifier: @"Login"];
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.vcLogin didLogined];            
        });
        
        
        DLog(@"UICore didLogined");
    }
}

-(void)loadSavedUser{
   [[UrMUser sharedUser] loadCustomObjectWithKey:kUser];
}

- (void)setLogined:(BOOL)status{
    [self.preferences setBool:status forKey:kLoginPref];
}

- (BOOL)isLogined{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
//    return [preferences boolForKey:kLoginPref];
    return YES;
}

- (void)didLoginFailed:(NSString*)statusMessage{
    [self hideSpinner];
    [self.vcLogin didLoginFailed:statusMessage];
}

- (void)logout{
    //TODO
    //[self.preferences setObject: forKey:kUser];
    [self setLogined:NO];
    if ([FBSDKAccessToken currentAccessToken]) {
    [[FBSDKLoginManager new] logOut];
    }
    bgTasksEnabled = NO;
    [[UrMUser sharedUser] logout];
//    sharedController = nil;
}

- (void)sendRestorePasswordForEmail:(NSString*)email completionHandler:(void (^)(NSString *status, NSString *message))handler{
    [database sendRestorePasswordForEmail:email completionHandler:handler];
}

- (void)sendChangePasswordWithData:(NSDictionary*)userData completionHandler:(void (^)(NSString *status, NSString *message))handler{
    [database sendChangePasswordWithData:userData completionHandler:^(NSString *status, NSString *message) {
        handler(status,message);
    }];
}

#pragma mark - Registration new User

- (void)registerUserWithData:(NSDictionary*)userData {
    [self showSpinner:self.vcNewUser];
    
    [database registerUserWithData:(NSDictionary*)userData];
}

- (void)didRegistrationLogined:(UrMUser *)user{
    [self hideSpinner];
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
    
    [self.preferences setObject:encodedObject forKey:kUser];
    [self setLogined:YES];
    //for push notification
    [self bindUserToDevice:user];
    [self.vcNewUser didRegistrationLogined];
}
- (void)didRegistrationLoginFailed{
    [self hideSpinner];
}

#pragma mark - Registration new Event
-(void)registerEvent:(UrMEvent*)event{
    [database registerEventWithData:[event newEventDictionary]];
}
-(void)didRegisterEvent:(NSString*)message{
    [self.vcEvent didRegisterEvent:message];
    DLog(@"didRegisterEvent");
}
-(void)didRegisterEventFailed:(NSString*)message{
    [self.vcEvent didRegisterEventFailed:message];
    DLog(@"didRegisterEventFailed");
}

#pragma mark - Contacts
-(void)searchContactsFor:(NSString*)name{
    NSMutableDictionary* searchData = [NSMutableDictionary new];
    
    [searchData setObject:name forKey:@"name"];
    [searchData setObject:[UrMUser sharedUser].guid forKey:@"userId"];
    [searchData setObject:@"" forKey:@"email"];
    [searchData setObject:kLimitRowsDefault forKey:@"returnRows"];
    
    [database searchContactsWithData:searchData];
}
-(void)didReceivedContacts:(NSArray*)contacts{
    [self.vcContacts didReceiveContacts:contacts];
}
-(void)contactsNotFound{
}
-(void)addContacts{
    if (self.vcPending) {
//        self.vcPending
    }
}

#pragma mark - Settings Title model methods

- (NSArray*)getSettingTitleDataForUser{
    return [_settingsTitleModel getDefaultUserSettingsTitleDictionary];
}

- (NSString*)getVCNameBySelectedChoice:(NSString*)name{
    return [_settingsTitleModel getVCNameBySelectedChoice:name];
}


#pragma mark - Music
-(void)loadTracksForEventId:(NSString *)eventId andUserId:(NSString *)ownerId completionHandler:(void (^)(NSMutableArray *tracks))handler{
    [database loadTracksForEventId:eventId andUserId:ownerId completionHandler:^(NSMutableArray *tracks) {
        handler(tracks);
    }];
}
-(void)sendMusicItem:(NSMutableArray*)musicItems forEventId:(NSString*)evenrId completionHandler:(void (^)(NSString *status, NSString *message))handler{
    for (UrMMusic *musicItem in musicItems) {
        if (musicItem.isInitial) continue;
        NSDictionary *musicItemData = [self dictForMusicItem:musicItem andEvent:evenrId];        
        [database uploadMusicItemWithData:musicItemData completionHandler:^(NSString *status, NSString *message) {
            DLog(@"CORE uploadMusicItemWithData status %@", status);
            handler(status, message);
        }];
    }
}
-(void)addMusicItem:(UrMMusic*)musicItem forEventId:(NSString*)evenrId completionHandler:(void (^)(NSString *status, NSString *message))handler{
    if (musicItem.isInitial) handler(@"Ok", @"");
    else{
        NSDictionary *musicItemData = [self dictForMusicItem:musicItem andEvent:evenrId];
        [database uploadMusicItemWithData:musicItemData completionHandler:^(NSString *status, NSString *message) {
            DLog(@"CORE addMusicItem status %@", status);
            handler(status, message);
        }];
    }

}

-(void)deleteMusicItem:(NSString*)musicItemId andUserId:(NSString*)userId completionHandler:(void (^)(NSString *status, NSString *message))handler{
    [database deleteMusicItem:musicItemId andUserId:userId completionHandler:^(NSString *status, NSString *message) {
            DLog(@"CORE deleteMusicItem status %@, id %@", status, musicItemId );
            handler(status, message);
        }];
}


-(void)approveMusicItem:(UrMMusic*)musicItem forEventId:(NSString*)evenrId completionHandler:(void (^)(NSString *status, NSString *message))handler{
        if (musicItem.isInitial) return;
        NSDictionary *musicItemData = [self dictForApproveMusicItem:musicItem andEvent:evenrId];
        [database approveMusicItemWithData:musicItemData completionHandler:^(NSString *status, NSString *message) {
            DLog(@"CORE approveMusicItem status %@", status);
            handler(status, message);
        }];
}

#pragma mark - User Notifications
- (void)loadNotificationsForUser:(NSString *)userId completionHandler:(void (^)(NSMutableArray *notifications))handler{
    [database loadNotificationsForUser:userId completionHandler:^(NSMutableArray *notifications) {
        DLog(@"CORE loadNotificationsForUser");
        handler(notifications);
    }];
}

#pragma mark - Edit Event
-(void)editEvent:(UrMEvent*)event{
    [self showSpinner:self.vcEventDetails];
    [database editEventWithData:[event editedEventDictionary]];
}

-(void)didEditedEvent{
    DLog(@"didEditedEventDone");
    [self hideSpinner];
    [self.vcEventDetails didEditedEventDone];

}
-(void)didEditedEventFailed:(NSString*)message{
    DLog(@"didEditedEventFailed %@", message);
    [self hideSpinner];
    [self.vcEventDetails didEditedEventFailed:message];

}
-(void)deleteEventWithUserId:(NSString*)userId andEventId:(NSString*)eventId{
    [database deleteEventWithUserId:userId andEventId:eventId];
}
-(void)didEventDeleted{
    [self.vcEventDetails didEventDeleted];
}
-(void)didEventDeletedFiled:(NSString*)message{
    [self.vcEventDetails didEventDeletedFiled:message];
}

#pragma mark - UploadImages
-(void) sendPictures:(NSMutableArray*)pictures forEventId:(NSString*)evenrId{
    [self showSpinner:self.vcEventDetails];
    
    for (UrMPicture* picture in pictures) {
        if (!picture.isInitial && !picture.imageId) {
            
            NSMutableDictionary *dictForJSON = [NSMutableDictionary dictionary];
            
            if (picture.isAvatar) [dictForJSON setObject:@"true" forKey:@"isAvatar"];
            else [dictForJSON setObject:@"false" forKey:@"isAvatar"];
            
            [dictForJSON setObject:[UrMUser sharedUser].guid forKey:@"ownerId"];
            [dictForJSON setObject:evenrId forKey:@"eventId"];
            
            [dictForJSON setObject:picture.name forKey:@"fileName"];
            
            NSData *imgData = UIImagePNGRepresentation([self resizeImage:picture.image]);
            NSData *synth = [imgData base64EncodedDataWithOptions:0];//[[NSString alloc] initWithData:imgData];
            
            [dictForJSON setObject:[[NSString alloc] initWithData:synth encoding:NSUTF8StringEncoding] forKey:@"imageData"];
            [database uploadPictureWithData:dictForJSON];
            _imagesToUpload++;
            DLog(@"images %li",(long)_imagesToUpload);
            
            //makr picture as not new..
            picture.imageId = @"newId";
        }
    }
}

-(void)uploadPictureDone{
    [self hideSpinner];
    
    _imagesToUpload--;
     DLog(@"images %li",(long)_imagesToUpload);
    if (_imagesToUpload == 0) {
        DLog(@"images uploaded");
//        [self.vcEvent uploadPictureDone];
        [self.currentViewConttoller uploadPictureDone];
    }
}
-(void)uploadPictureFiled:(NSString*)message{
//    [self.vcEvent uploadPictureFiled:message];
    [self.currentViewConttoller uploadPictureFiled:message];
    [self hideSpinner];
}

-(void)sendPictureState:(UrMPicture*)pictureEntity{
    [self showSpinner:self.vcPictureGallery];
    if (pictureEntity) {
        NSMutableDictionary *dictForJSON = [NSMutableDictionary dictionary];
        NSString* status = [NSString stringWithFormat:@"%u", pictureEntity.approved_state];
        
        [dictForJSON setObject:pictureEntity.imageId forKey:@"imageId"];
        [dictForJSON setObject:[UrMUser sharedUser].guid forKey:@"eventOwnerId"];
        [dictForJSON setObject:status forKey:@"isApproved"];
        
        [database sendPictureState:dictForJSON];
    }else [self pictureStateUpdatedFiled:@"Picture error"];
}

-(void)pictureStateUpdated{
    [self hideSpinner];
    [self.vcPictureGallery pictureStateUpdated];
}
-(void)pictureStateUpdatedFiled:(NSString*)message{
    [self hideSpinner];
    DLog(@"Error pictureStateUpdated: %@", message);
    [self.vcPictureGallery pictureStateUpdatedFiled:message];
}

-(void)sendPictureState:(UrMPicture*)pictureEntity completionHandler:(void (^)(NSString *status, NSString *message))handler{
    if (pictureEntity) {
        NSMutableDictionary *dictForJSON = [NSMutableDictionary dictionary];
        NSString* status = [NSString stringWithFormat:@"%u", pictureEntity.approved_state];
        
        [dictForJSON setObject:pictureEntity.imageId forKey:@"imageId"];
        [dictForJSON setObject:[UrMUser sharedUser].guid forKey:@"eventOwnerId"];
        [dictForJSON setObject:status forKey:@"isApproved"];
        
        [database sendPictureState:dictForJSON completionHandler:^(NSString *status, NSString *message) {
            DLog(@"CORE sendPictureState status %@", status);
            handler(status, message);
        }];
    }
}

-(void)deletePicture:(NSString*)imageId{
    [self showSpinner:self.vcEventDetails];
    [database deletePicture: imageId];
}
-(void)didPictureDeleted{
    [self hideSpinner];
    [self.vcEventDetails didPictureDeleted];
}
-(void)didPictureDeletedFiled:(NSString*)message{
    [self hideSpinner];
    [self.vcEventDetails didPictureDeletedFiled:message];
}

#pragma mark - Profile

- (void)saveAvatarPicture:(UIImage*)avatarPicture{
    [UrMUser sharedUser].avatarImage = avatarPicture;
    NSData *imgData = UIImagePNGRepresentation(avatarPicture);
    NSData *synth = [imgData base64EncodedDataWithOptions:0];
    
    NSMutableDictionary *dictForJSON = [NSMutableDictionary dictionary];
    [dictForJSON setObject:[UrMUser sharedUser].name forKey:@"fileName"];
    [dictForJSON setObject:[[NSString alloc] initWithData:synth encoding:NSUTF8StringEncoding] forKey:@"content"];
    
    [database saveAvatarPicture:dictForJSON];
}

-(UIImage*)loadAvatarPicture{
    if(![UrMUser sharedUser].avatarImage){
        [UrMUser sharedUser].avatarImage = [self loadImageFromURL:[UrMUser sharedUser].avatarURL];
    }
    
    return [UrMUser sharedUser].avatarImage;
}

- (void)saveUserProfileWithData:(NSDictionary*)userData{
    [database saveUserProfileWithData:userData];
}

#pragma mark - LoadEvents for user
-(void)loadEventsForUser{
    [self showSpinner:self.vcHome];
    NSString* userID = [UrMUser sharedUser].guid;
    [database loadEventsForUser:userID];
}
-(void)didLoadEvents:(NSMutableArray*)eventsList{
    [self hideSpinner];
    [self.vcHome didLoadEvents:eventsList];
}
-(void)didLoadEventsEmpty{
    [self hideSpinner];
}

#pragma mark - Event Details
-(void)loadEventDetails{
    if(self.eventForDetails){
        [self didLoadEventDetails:self.eventForDetails];
    }else{
        if(self.externalEventId){
            [database loadEventById:self.externalEventId];
        }
    }
}

-(void)didLoadEventDetails:(UrMEvent *)event{
    //show Details View
    if (self.vcEventDetails) {
        [self.vcEventDetails detailsDidLoaded:event];
    }
}

-(void)cleanEventForDetails{
    self.eventForDetails = nil;
    self.externalEventId = nil;
}

-(void)eventForLoadDetails:(UrMEvent *) event{
    if (event) {
        self.eventForDetails = event;
    }
}

-(void)changeInviteStatus:(NSString*)status forUser:(NSString*)userId forEvent:(NSString*)eventId{
    [self showSpinner:self.vcEventDetails];
    [database changeInviteStatus:status forUser:userId forEvent:eventId];
}
-(void)inviteStatusUpdated{
    [self hideSpinner];
    [self.vcEventDetails inviteStatusChanged];
}
-(void)inviteStatusUpdatedFailed{
    [self hideSpinner];
}

#pragma mark - Push Notifications

-(void) bindUserToDevice:(UrMUser *)user{
    
    // Register user to Push Notification center OneSignal
    [OneSignal registerForPushNotifications];
    
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        if(pushToken) {
            NSLog(@"Received push token - %@", pushToken);
            NSLog(@"User (Device) ID - %@", userId);
            
            // send device Info to NLT server
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self sendPushBindForUserId: user.guid
                            andUserDeviceId: userId
                              andDeviceType: [self getDeviceType]];
            });
        }else{
            DLog(@"Received push token nil. The response may be nil if the user does not accept push notifications for your app or there was a connection issue.");
        }
    }];
    [OneSignal getTags:^(NSDictionary* result) {
        NSLog(@"%@", result);
    }];
}

#pragma mark - Utils

-(UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 800.0;
    float maxWidth = 600.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}

-(UIImage*)loadImageFromURL:(NSString*)url{    
    return [UrImageAsyncLoader loadImageSyncFromURL:url];
}

-(void)loadImageByURL:(NSString*)url forButton:(UIButton*)button{
   [UrImageAsyncLoader load:button buttonImageWithUrlString:url];
}

-(void)loadImageByURL:(NSString*)url forImageView:(UIImageView*)imageView{
    [UrImageAsyncLoader load:imageView imageWithUrlString:url];
}

-(void)load:(UIImageView *)imageView imageWithUrlString:(NSString *)urlString completionHandler:(void (^)(UIImage *image))handler{
        [UrImageAsyncLoader load:imageView imageWithUrlString:urlString completionHandler:^(UIImage *image) {
            handler(image);
        }];
}

-(void)loadButtonImage:(UIButton*)button forPicture:(UrMPicture*)picture{
    [UrImageAsyncLoader load:button buttonImageWithPicture:picture];
}
-(void)loadViewImage:(UIImageView*)imageView forPicture:(UrMPicture*)picture{
    [UrImageAsyncLoader load:imageView imageWithPicture:picture];
}


-(NSString *)getDeviceType{
    NSString* deviceType;
    if(IS_IPAD) deviceType = @"iPAD";
    else deviceType = @"iPhone";
    return deviceType;
}


-(void)getUserCalendarTime:(NSDate*)date{
    [self calendarEventsByDate:date];
}

-(void)didloadCalendarEvents:(NSMutableArray*)events{
    [self.vcEvent eventsDidLoaded:events];
}

-(NSDictionary*)dictForMusicItem:(UrMMusic*)musicItem andEvent:(NSString *)eventId{
    NSDictionary* data = [NSMutableDictionary dictionary];
    data = @{@"ownerId"     : [UrMUser sharedUser].guid,
             @"eventId"     : eventId,
             @"isApproved"  : [NSString stringWithFormat:@"%i", track_state_new],
             @"name"        : musicItem.name,
             @"url"         : musicItem.uri,
             @"spotifyType" : [NSString stringWithFormat:@"%i", musicItem.itemType],
             @"artist"      : musicItem.artist,
             @"album"       : musicItem.album,
             @"imageUriLow" : musicItem.covers[@"imageUriLow"],
             @"imageUriMed" : musicItem.covers[@"imageUriMed"],
             @"imageUriHigh": musicItem.covers[@"imageUriHigh"]};
    
    return data;
}
-(NSDictionary*)dictForApproveMusicItem:(UrMMusic*)musicItem andEvent:(NSString *)eventId{
    NSDictionary* data = [NSMutableDictionary dictionary];
    NSString* status = [NSString stringWithFormat:@"%u", musicItem.track_state];
    
    data = @{@"userId"  : [UrMUser sharedUser].guid,
             @"eventId" : eventId,
             @"status"  : status,
             @"trackId" : musicItem.itemId };
    
    return data;
}

#pragma mark Internet Connection
- (void)checkInternetConnection{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.hostReachability = [UUReachabilityConnection reachabilityWithHostName:kHostAdress];
    [self.hostReachability startNotifier];
}

- (void)updateInterfaceWithReachability:(UUReachabilityConnection *)reachability{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
    switch (netStatus)
    {
        case NotReachable:        {
            DLog(@"NO Connect");
//            self.localDB.isOnline = NO;
            break;
        }
        case ReachableViaWWAN:        {
            DLog(@"Connected WWAN");
//            self.localDB.isOnline = YES;
            break;
        }
        case ReachableViaWiFi:        {
            DLog(@"Connected WiFi");
//            self.localDB.isOnline = YES;
            break;
        }
    }
}

- (void) reachabilityChanged:(NSNotification *)note{
    UUReachabilityConnection* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[UUReachabilityConnection class]]);
    [self updateInterfaceWithReachability:curReach];
}
- (BOOL)isOnline{
//    return self.localDB.isOnline;
    return YES;
}

-(NSInteger) correctPictureCellIndex:(NSInteger)rawCellIndex for:(NSMutableArray*)pictures {
    NSInteger currentIndex = rawCellIndex;
    
    for (int i = (int)[pictures count]-1; i >= 0; i--) {
        UrMPicture* picture = pictures[i];
        if (picture.isInitial || picture.isAvatar){
            if (i < currentIndex) {
                currentIndex--;
            }
        }
    }
    return currentIndex;
}


-(NSMutableArray*) clearPicturesForInitialAndAvatar:(NSMutableArray*)pictures{
    NSMutableArray* result = [NSMutableArray array];
    for (int i = 0; i < [pictures count]; i++) {
        UrMPicture* picture = pictures[i];
//        if (!picture.isInitial && !picture.isAvatar)[result addObject:pictures[i]];
        if (!picture.isInitial && !picture.isDefault)[result addObject:pictures[i]];
    }
    return result;
}

#pragma mark - Date Calculations


-(void)calendarEventsByDate:(NSDate *)date{
    self.eventsOnThisDay = [NSMutableArray new];
        NSDate *startDate = [self dateAtBeginningOfDayForDate:date];
        NSDate *endDate = [self dateByAddingHours:23 andMinutes:59 toDate:date];

    
        EKEventStore *eventStore = [[EKEventStore alloc] init];
        EKEntityType type = EKEntityTypeEvent; //EKEntityTypeReminder or EKEntityTypeEvent
        [eventStore requestAccessToEntityType:type completion:^(BOOL granted, NSError *error) {
            if (granted) {
                NSArray * calendars = [eventStore calendarsForEntityType:type];
                NSPredicate *searchPredicate = [eventStore predicateForEventsWithStartDate:startDate endDate:endDate calendars:calendars];
                NSArray *events = [eventStore eventsMatchingPredicate:searchPredicate];
                

                for (EKEvent *event in events){

//                    NSDate *dateRepresentingThisDay = [self dateAtBeginningOfDayForDate:event.startDate];

//                    NSMutableArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
//                    if (eventsOnThisDay == nil) {
//                        eventsOnThisDay = [NSMutableArray array];//
//                        // Use the reduced date as dictionary key to later retrieve the event list this day
//                        [self.sections setObject:eventsOnThisDay forKey:dateRepresentingThisDay];//
//                    }//

                    [self.eventsOnThisDay addObject:event];
                    
                    DLog(@"event.title %@", event.title);
                    DLog(@"start startDate %@", event.startDate);
                    DLog(@"start endDate %@", event.endDate);
                }
                [self.vcEvent eventsDidLoaded:self.eventsOnThisDay];
            }
        }
         ];
}

-(void) createEvent{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = @"Event Title";
        event.startDate = [NSDate date]; //today
        event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        //self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
    }];
}

-(void) deleteEvetntBy:(id)eventId{
    EKEventStore* store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent* eventToRemove = [store eventWithIdentifier:eventId];
        if (eventToRemove) {
            NSError* error = nil;
            [store removeEvent:eventToRemove span:EKSpanThisEvent commit:YES error:&error];
        }
    }];
}

- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (NSDate *)dateAtEndingOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:23];
    [dateComps setMinute:59];
    [dateComps setSecond:59];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (NSDate *)dateWithTime:(NSString*)timeString fromDate:(NSDate *)inputDate{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:inputDate];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH : mm"];
    NSDate *dateTime= [formatter dateFromString:timeString];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:dateTime];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    // Set the time components manually
    [dateComps setHour:hour];
    [dateComps setMinute:minute];
    [dateComps setSecond:00];
    
    // Convert back
    NSDate *rezultDay = [calendar dateFromComponents:dateComps];
    return rezultDay;
}


- (NSDate *)dateWithEndHourTime:(NSString*)timeString fromDate:(NSDate *)inputDate{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:inputDate];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH : mm"];
    NSDate *dateTime= [formatter dateFromString:timeString];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:dateTime];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    // Set the time components manually
    [dateComps setHour:hour];
    
    if (minute == 30){
        [dateComps setMinute:59];
    }
    if (minute == 00){
        [dateComps setMinute:29];
    }
    
    [dateComps setSecond:59];
    
    // Convert back
    NSDate *rezultDay = [calendar dateFromComponents:dateComps];
    return rezultDay;
}

- (NSDate *)dateByAddingHours:(NSInteger)hours andMinutes:(NSInteger)minutes toDate:(NSDate *)inputDate
{
    // Use the user's current calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
//    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:inputDate];

    [dateComps setHour:hours];
    [dateComps setMinute:minutes];
    [dateComps setSecond:0];
    
//    NSDate *newDate = [calendar dateByAddingComponents:dateComps toDate:inputDate options:0];
    
    NSDate *endingOfDay = [calendar dateFromComponents:dateComps];
    return endingOfDay;
}

- (NSDate *)dateByAddingYears:(NSInteger)numberOfYears toDate:(NSDate *)inputDate
{
    // Use the user's current calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setYear:numberOfYears];
    
    NSDate *newDate = [calendar dateByAddingComponents:dateComps toDate:inputDate options:0];
    return newDate;
}


#pragma mark - Push Notification
-(void)sendPushBindForUserId: (NSString*)userId
             andUserDeviceId: (NSString*)deviceId
               andDeviceType: (NSString*)deviceType{
    
    NSMutableDictionary *dictForJSON = [NSMutableDictionary dictionary];
    
    [dictForJSON setObject:deviceId forKey:@"DeviceId"];
    [dictForJSON setObject:deviceType forKey:@"Platform"];
    
    [database sendPushBindForUserId:userId withData:dictForJSON];
}

#pragma mark - Spinner
-(void)showSpinner:(UIViewController*)viewController{
    self.spinner = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(135,140,100,100)];
    self.spinner.center = viewController.view.center;
    self.spinner.color = [UIColor redColor];
    [self.spinner startAnimating];
    [viewController.view addSubview:self.spinner];
}

-(void)hideSpinner{

    dispatch_async(dispatch_get_main_queue(), ^{
        // stop and remove the spinner on the main thread when done
        if (self.spinner) {
            [self.spinner removeFromSuperview];
        }
    });

}

-(void)checkNotification{
    if (![[NSUserDefaults standardUserDefaults]objectForKey:kPushNotificationData]) return;
    
    [[UUNotificationManager new] parseData];
}

#pragma mark - Tags
-(void)getUserTags:(NSString*)userID completionHandler:(void (^)(NSMutableArray *tags))handler{
    [database getUserTags:userID completionHandler:^(NSMutableArray *tags) {       
        handler(tags);
    }];
}

-(void)getEventTags:(NSString*)eventID completionHandler:(void (^)(NSMutableArray *tags))handler{
    [database getEventTags:eventID completionHandler:^(NSMutableArray *tags) {
        handler(tags);
    }];
}

-(void)action:(int)actionCode forTags:(NSMutableArray*)tags completionHandler:(void (^)(NSString *status, NSString *message))handler{
    NSDictionary *userTagDataSend = [NSDictionary dictionary];
    
    userTagDataSend = @{@"userId"  : [UrMUser sharedUser].guid,
                        @"tags"    : tags,
                        @"action"  : [NSString stringWithFormat:@"%u", actionCode]};
    
    [database actionUserTag:userTagDataSend completionHandler:^(NSString *status, NSString *message) {
        handler(status, message);
    }];
}

-(void)action:(int)actionCode forTags:(NSMutableArray*)tags forEvent:(NSString*)eventID completionHandler:(void (^)(NSString *status, NSString *message))handler{
    NSDictionary *eventTagDataSend = [NSDictionary dictionary];
    
    eventTagDataSend = @{@"userId"  : [UrMUser sharedUser].guid,
                         @"tags"    : tags,
                         @"eventId" : eventID,
                         @"action"  : [NSString stringWithFormat:@"%u", actionCode]};
    
    [database actionEventTag:eventTagDataSend completionHandler:^(NSString *status, NSString *message) {
        handler(status, message);
    }];
}

#pragma mark - ArkhiveData

#pragma mark - Background Tasks
-(void)startBackgroundTasks{
    bgTasksEnabled = YES;
    [self getCountOfNotifications];
}

-(void)getCountOfNotifications{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if ([UrMUser sharedUser].guid) [database getCountOfNotifications:[UrMUser sharedUser].guid];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kLoadNotificationDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(bgTasksEnabled) [self getCountOfNotifications];
    });
}

-(void)didLoadNotificationsCount:(int)count{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.currentViewConttoller) {
            if (count > 0) {
                DLog(@"CORE createNotifyButton");
                [self.currentViewConttoller createNotifyButton];
            } else {
                if ((self.notifyButton.superview)) {
                    [self.notifyButton removeFromSuperview];
                    self.notifyButton = nil;
                }
            }
        }else{
            DLog(@"CORE currentViewConttoller nill");
        }
    });
    //TODO Show notification red indicator
}

@end
