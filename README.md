# sdklib

[![CI Status](http://img.shields.io/travis/Aleksey Kislenko/sdklib.svg?style=flat)](https://travis-ci.org/Aleksey Kislenko/sdklib)
[![Version](https://img.shields.io/cocoapods/v/sdklib.svg?style=flat)](http://cocoapods.org/pods/sdklib)
[![License](https://img.shields.io/cocoapods/l/sdklib.svg?style=flat)](http://cocoapods.org/pods/sdklib)
[![Platform](https://img.shields.io/cocoapods/p/sdklib.svg?style=flat)](http://cocoapods.org/pods/sdklib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

sdklib is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'sdklib'
```

## Author

Aleksey Kislenko, a.kislenko@newline.tech

## License

sdklib is available under the MIT license. See the LICENSE file for more info.
