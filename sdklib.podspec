#
# Be sure to run `pod lib lint sdklib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'sdklib'
  s.version          = '0.1.0'
  s.summary          = 'This description is used'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'This description is used to generate tags and improve search results'

  s.homepage         = 'https://bitbucket.org/AlekseyKislenko/sdklib'
  # s.screenshots     = 'https://png.pngtree.com/element_origin_min_pic/16/07/22/2057921811589a1.jpg'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Aleksey Kislenko' => 'a.kislenko@newline.tech' }
  s.source           = { :git => 'https://bitbucket.org/AlekseyKislenko/sdklib', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'sdklib/Classes/**/*'
  
  # s.resource_bundles = {
  #   'sdklib' => ['sdklib/Assets/*.png']
  # }

  # s.public_header_files = 'sdklib/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
